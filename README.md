# Apple computer stock Challenge 1 SQL

----
## Stored SQL procedure

> This code is designed and stored to be reused to return the Volume Weighted Average Price in a specified 5 hour interval at any date requested by the user.

You will find the mentioned code in:

*(sqltask.sql)*

----
## Input 

> To execute the stored procedure, input:
**EXEC usp_CalculateVWAP yyyymmddhhmm**
where
yyyy=year, mm=month, dd-day, hh-hour, mm-minute.
For example: EXEC usp_CalculateVWAP 201008170900 which stands for the 17th of August 2010 at 09:00 am.

##Output

> Gives the calculated Volume Weighted Average price for the specified 5 hours interval at the specified date.

## Assumptions
1. Note that the price chosen to calculate the VWAP is assumed to be the close price as a trade takes time to settle.
2. An interval of 5 hours is assumed, meaning that if the markets close before the interval is completed, the VAWP will reflect a full 5 hours continued from the time the markets open again (usually the next day).
3. It is assumed that the date has to always be in a yyyy/mm/dd format.


