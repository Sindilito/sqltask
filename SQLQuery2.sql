create procedure
select distinct top 3
[date]
,max(abs([close]-[open])) as [range] 
INTO #range
FROM task2
GROUP BY [date]
order by [range] desc;
 drop table #range


select *
from #range

select 
[date]
,max(high) as maxhigh
INTO #maxhigh
FROM task2

Where [date] IN (select [date] from #range)
GROUP BY [date]


select *
from #maxhigh

select 
t.date
,[time]
INTO #timemaxhigh
FROM task2 t JOIN #maxhigh m ON m.maxhigh = t.high 
AND t.date = m.date

select *
from #timemaxhigh

select
r.date
,r.range
,tm.time
FROM #range r JOIN #timemaxhigh tm ON r.date=tm.dates