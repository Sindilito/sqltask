# IBM trading price range challenge 2 SQL

----
## Stored procedure

> This code is designed and stored to be reused to return the top 3 dates with the biggest range of trade prices and the exact time at which the price reached maximum in these specified 3 dates.

You will find the mentioned code in:

*(SQLQuery2.sql)*

----


##Output

> Gives the 3 dates in which trading prices had the biggest range in descending order. It specified exactly what the range was. It also provides the exact time at which the trade price was at its peak.

## Assumptions
1. To calculate the range I have taken the absolute value of the difference between open and close trade prices. Note that this might not necessarily provide a good overview on how much the prices have fluctuated as close price could match open price at the end of the day even if there have been many changes throughout the day.
