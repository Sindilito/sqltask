DECLARE @date char(12)
DECLARE @enddate char(12)
set @Date = '201010110900'
set @enddate = (select cast(@date as numeric(20,0))+ 500)

SELECT
SUM(VOLUME*CLOSE_PRICE)/SUM(VOLUME) AS VOLUME_WEIGHTED_PRICE
,substring(@date,7,2)+'/'+substring(@date,5,2)+'/'+left(@date,4) as [date]
,left(right(@Date,4),2)+':'+right(@Date,2) + '-' + left(right(@enddate,4),2)+':'+right(@enddate,2) as interval
FROM tableapple
WHERE [date] between @date and @enddate;


--ASSUMPTION TO PICK CLOSE PRICE AS THE PRICE OF THE TRADE BECAUSE ONCE IT IS SETTLED IT TAKES TIME
--ASSUMPTION ON TIME MAYBE

